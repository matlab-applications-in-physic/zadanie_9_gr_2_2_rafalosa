clear;
clc;

myFileName = 'z3_1350V_x20.dat';
myFile = dir(myFileName);
myFileId=fopen(myFileName);

blockSize = 500000;
screenSize = 2000;


data = fread(myFileId,blockSize,'uint8');

fclose(myFileId);

    
X = linspace(0,blockSize-1,blockSize);

plot(X,data - 117);

pulses = CountPulses(data - 117);
 pos = ['Pulses:',num2str(pulses)];
text(blockSize/2,20,pos);

