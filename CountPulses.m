
%Rafa� Osadnik
%Engineering Physics
%Institute of Physics

function [Pulses] = CountPulses(Signal)

%This method of counting pulses, depends on dividing the signal file into
%several fragments, each 100 bytes (50 ns) long. The function then parses
%through each fragment and checks for values that are below and above a
%certain value. If there is a occurence of a value less than a negatie
%value of the threshold, then the entire fragment is scrapped and counts as
%noise. If there are none such occurences in a given fragment then it is
%integrated and depending on the value of the integral counted as a
%singular pulse.


SignalThreshold = 4; %Setting up a signal threshold value

IntegralThreshold = 10; %Setting up a integral value threshold

TimeStep = 1; %Numerical integral time step

Integral = 0;

Pulses = 0;

Resolution = 100; %Number of bytes for dividing the signal into fragments

Conditions = [false false];

for n = 1:fix(size(Signal,1)/Resolution) %Main loop that parses through fragments

    for k = (n-1)*Resolution + 1:(n-1)*Resolution + Resolution - 1 %Loop that looks for values above and below threshold
        
        if(Signal(k) > SignalThreshold), Conditions(1) = true;end 
                    
        if(Signal(k) < -SignalThreshold), Conditions(2) = true;end
        
    end
    
    if(Conditions(1) && ~Conditions(2)) %If there are none values below 
                                        %threshold and if there is at least
                                        %one value above
                                        %threshold, then proceed to
                                        %integrate the fragment
        
        for k = (n-1)*Resolution + 1:(n-1)*Resolution + Resolution - 1
            
            Integral = Integral + TimeStep * Signal(k); %Numerically integrating the signal
  
        end
        
        if(Integral >= IntegralThreshold), Pulses = Pulses + 1;end %If the integral value is above a certain value, then it counts as a pulse
        Integral = 0;  
    end
    
    Conditions = [false false];  %Reseting conditions
    
end

end





