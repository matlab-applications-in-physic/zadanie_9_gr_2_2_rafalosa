
%Rafa� Osadnik
%Engineering physics
%Institute of physics

fileName = 'z3_1350V_x20.dat';

MatlabInfo = memory;

MaxMatrixSize = MatlabInfo.MaxPossibleArrayBytes / 40; % Matlab RAM usage can spike up to 3.5GB please take that into consideration

FileInfo = dir('z3_1350V_x20.dat');

FileSize = FileInfo.bytes;

FileID = fopen('z3_1350V_x20.dat');

NoiseLevelComponents = zeros(256,1);

for n = 1 : fix(FileSize/MaxMatrixSize)+1 %Dividing the file into chunks and processing them
    
    DataChunk = fread(FileID,MaxMatrixSize,'uint8');
    NoiseLevelComponents = NoiseLevelComponents + NoiseLevel(DataChunk);
    clear DataChunk;
    
end

fclose(FileID);

Values = linspace(0,255,256);

bar(Values + 1, NoiseLevelComponents);

saveas(gcf,'z3_1350V_x20_histogram.pdf');

xlabel('ADC voltage [q]')
ylabel('Number of occurences')

[Events,FinalNoiseLevel] = max(NoiseLevelComponents); 

FileID = fopen('z3_1350V_x20.dat');

TotalPulses = 0;

for n = 1 : fix(FileSize/MaxMatrixSize)+1
    
    DataChunk = fread(FileID,MaxMatrixSize,'uint8');
    TotalPulses = TotalPulses + CountPulses(DataChunk - FinalNoiseLevel);
    clear DataChunk;
    
end

fclose(FileID);

DataFile = fopen('z3_1350V_x20_peak_analysis_results.dat ','w');

fprintf(DataFile,'Noise level: %d \nPulses: %d \nPulses uncertainty: %f \n',FinalNoiseLevel, TotalPulses, sqrt(TotalPulses) );

fclose(DataFile);




