
%Rafa� Osadnik
%Engineering Physics
%Institute of Physics

function [Component] = NoiseLevel(Data)
    
    Component = zeros(256,1);

    for k = 1:size(Data,1)
        
        Component(Data(k)) = Component(Data(k)) + 1;
        
    end
    
end

